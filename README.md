# Using long read transcriptome:

[Quality control of isoseq reads](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Isoseq3-quality-control)  

[Assembled _de novo_ transcriptome](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Reference-transcriptome)  
       -  [Check for fragmented reads](https://gitlab.com/douglas-fir-transcriptome/Check-for-fragmented-reads)

[Predicted long non-coding RNA from _de novo_ transcriptome assembly](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Predict-lncRNA)  

[Identify miRNA](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Identify-miRNA)  

[Identified coding transcription factors](https://gitlab.com/douglas-fir-transcriptome/de-novo-assembly-of-long-reads/tree/Identify-TF)   
